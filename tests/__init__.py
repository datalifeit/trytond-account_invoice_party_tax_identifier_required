# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_account_invoice_party_tax_identifier_required import suite

__all__ = ['suite']
